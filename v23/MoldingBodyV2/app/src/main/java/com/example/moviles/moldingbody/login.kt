package com.example.moviles.moldingbody


import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.support.v4.media.session.PlaybackStateCompat
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_registrate.*



class login : Fragment(), GoogleApiClient.OnConnectionFailedListener  {
    override fun onConnectionFailed(p0: ConnectionResult) {

        Toast.makeText(context,"Error en la autenticación"+p0.errorMessage, Toast.LENGTH_LONG).show()

    }

    internal lateinit var botonGmail: Button
    internal lateinit var  botonIngresar:Button

    lateinit var mGoogleApiClient: GoogleApiClient

    private lateinit var txtUser: TextInputLayout
    private lateinit var txtPassword:TextInputLayout
    private lateinit var auth: FirebaseAuth
    private lateinit var txtForgetPassword:TextView
    lateinit var nombreBase:String
    lateinit var datosContador:String

    var db = FirebaseFirestore.getInstance()

    var contador= 0
    var newcontador=0


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        botonGmail= view.run { findViewById(R.id.gmail) }
        txtUser= view.run { findViewById(R.id.Ususario) }
        txtPassword=view.findViewById(R.id.Contrasenia)
        txtForgetPassword=view.findViewById(R.id.olvidarContrasenia)
        botonIngresar=view.findViewById(R.id.ingresar)

        botonIngresar.setOnClickListener{
            recuperarData()
            //contador=newcontador
            nombreBase="usuarioProducto${contador}"
            Log.d("usuariosContador",contador.toString())
            loginUser()
            addContador()}

        txtForgetPassword.setOnClickListener{olvidarContrasenia()}
        configureGoogleClient()

        botonGmail.setOnClickListener{
            recuperarData()
            contador=newcontador
            nombreBase="usuarioProducto${contador}"
            Log.d("usuariosContador",contador.toString())
            singIn()
            addContador()}


        auth=FirebaseAuth.getInstance()
    }

    fun addContador(){
        val cont = Contador(
            nombreBase,
            contador
        )
        db.collection("contador")
            .add(cont)
            .addOnSuccessListener { documentReference ->
                Log.d(
                    ContentValues.TAG,
                    "DocumentSnapshot added with ID: " + documentReference.id
                )
            }
            .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error adding document", e) }
    }

    fun recuperarData(){

        db.collection("contador")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d(ContentValues.TAG, "${document.id} => ${document.data}")
                    datosContador=document.data.toString()
                    val stringArray: List<String> = datosContador.split(",")
                    val stringValor: List<String> = (stringArray[1]).split("=")
                    val Valor: List<String> = (stringValor[1]).split("}")
                    newcontador =((Valor[0]).toInt())+1
                    //mando a la base
                    Log.d("newContador",newcontador.toString())
                }
                contador=newcontador
                Log.d("Contador",datosContador)
                Log.d("nuevoContador",contador.toString())
            }
    }

    private fun loginUser(){
        val user:String=txtUser.editText?.text.toString()
        val password:String=txtPassword.editText?.text.toString()
        if(!TextUtils.isEmpty(user)&&!TextUtils.isEmpty(password)){
            auth.signInWithEmailAndPassword(user,password)
                .addOnCompleteListener(OnCompleteListener(){
                    task ->
                    if(task.isSuccessful){
                        goToProductos()
                    }else{
                        Toast.makeText(context,"Error en la autenticación", Toast.LENGTH_LONG).show()
                    }
                })
        }
    }

    fun goToProductos(){
        val action=loginDirections.loginProductos(nombreBase)
        view?.findNavController()?.navigate(action)
    }

    fun olvidarContrasenia(){
       val action=loginDirections.olvidarContrasenia()
       view?.findNavController()?.navigate(action)
    }

    companion object{
        private val PERMISSION_CODE=9999

    }

    private fun singIn() {
        val intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(intent, PERMISSION_CODE)
    }

    private fun configureGoogleClient(){

        val options= GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleApiClient = context?.let {
            GoogleApiClient.Builder(it)
                .enableAutoManage(context as FragmentActivity,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,options)
                .build()
        }!!
        mGoogleApiClient.connect()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode== PERMISSION_CODE){
            Log.d("logingmail", data.toString())
            val result= Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            Log.d("logingmail", result.status.toString())
            if (result.isSuccess){

                val account: GoogleSignInAccount? = result.signInAccount
                val idToken: String? = account!!.idToken
                val credential = GoogleAuthProvider.getCredential(idToken,null)
                firebaseAuthwithGoogle(credential)

            }else
            {
                Toast.makeText(context,"Login uncessufull", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun firebaseAuthwithGoogle(credential: AuthCredential?) {
        auth.signInWithCredential(credential!!)
            .addOnSuccessListener {authResult ->
                val logged_email:String? = authResult.user.email
                val action=loginDirections.loginProductos(nombreBase)
                view?.findNavController()?.navigate(action)
            }
            .addOnFailureListener{
                    e->Toast.makeText(context,""+e.message, Toast.LENGTH_LONG).show()
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }
}
