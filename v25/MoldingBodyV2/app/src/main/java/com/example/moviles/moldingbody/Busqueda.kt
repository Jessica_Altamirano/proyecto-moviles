package com.example.moviles.moldingbody


import android.annotation.SuppressLint
import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_busqueda.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Busqueda : Fragment() {

    private val args: BusquedaArgs by this.navArgs()
    internal lateinit var nombreProd:TextView
    internal lateinit var descripcionProd:TextView
    internal lateinit var precioProd:TextView
    internal lateinit var imageProd:ImageView
    lateinit var Busca:ImageButton
    var db = FirebaseFirestore.getInstance()
    var enlace:String=""
    var nameBase:String=""
    internal lateinit var nombreBase: String

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        nombreProd=view.findViewById(R.id.nombre)
        descripcionProd=view.findViewById(R.id.descripcion)
        precioProd=view.findViewById(R.id.precio)
        imageProd=view.findViewById(R.id.imagen)
        Busca=view.findViewById(R.id.botonBusqueda)
        Busca.setOnClickListener {
            Log.d("BuquedaNombre", busqueda.text.toString())
            val producto = "productos"


            db.collection(producto)
                .whereEqualTo("nombre", busqueda.text.toString())
                .get()
                .addOnSuccessListener {documents ->
                    for (document in documents) {
                        Log.d(ContentValues.TAG, "${document.id} => ${document.data}")
                        val datosBusqueda=document.data.toString()
                        val stringArray: List<String> = datosBusqueda.split(",")

                        val nombre: List<String> = stringArray[0].split("=")
                        nombreProd.text=nombre[1]

                        val imagenProducto: List<String> = stringArray[1].split("=")
                        val tamanio=(imagenProducto).size
                        enlace=imagenProducto[1]
                        for(num in 2..(tamanio-1)) {
                            enlace=enlace+"="+imagenProducto[num]
                        }
                        Glide.with(this)
                            .load(enlace)
                            .into(imageProd)


                        val descripcion: List<String> = stringArray[2].split("=")
                        descripcionProd.text=descripcion[1]

                        val precio: List<String> = stringArray[3].split("=")
                        val precioSinLlave: List<String> = precio[1].split("}")
                        precioProd.text="Precio: $"+precioSinLlave[0]

                        imageProd.setOnClickListener{
                            nameBase="$enlace,${nombreProd.text},${precioSinLlave[0]},${descripcionProd.text}"
                            goToInformacion()
                            }
                    }
                }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error adding document", e) }
        }
    }
    fun goToInformacion(){
        nombreBase=args.nombreBase
        val action=BusquedaDirections.busquedaInformacion(nameBase,nombreBase)
        view?.findNavController()?.navigate(action)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_busqueda, container, false)
    }


}
