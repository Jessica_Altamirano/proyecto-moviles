package com.example.moviles.moldingbody


import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
/**
 * A simple [Fragment] subclass.
 *
 */
class editarCarrito : Fragment() {

    internal lateinit var gallery: LinearLayout
    internal lateinit var inflater: LayoutInflater
    internal lateinit var view2: View
    internal lateinit var nombre: TextView
    internal lateinit var descripcion: TextView

    internal lateinit var precio: TextView
    internal lateinit var imageView: ImageView
    internal lateinit var botonEliminar: Button

    var db = FirebaseFirestore.getInstance()
    var enlace:String=""

    var nompreProductoBorrar:String=""

    companion object {
        fun newInstance( base: String) :editarCarrito{
            val args = Bundle()
            args.putString(ARG_BASE, base)
            nombrebaseCarrito=base
            val fragment = editarCarrito()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        gallery= view.findViewById(R.id.producto)
        inflater =LayoutInflater.from(context)

        db.collection(nombrebase)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d(ContentValues.TAG, "${document.id} => ${document.data}")
                    datos=document.data.toString()

                    val stringArray: List<String> = datos.split(",")

                    view2=inflater.inflate(R.layout.editarcarro,gallery, false)
                    val nameProducto: List<String> = stringArray[0].split("=")
                    nombre=view2.findViewById(R.id.nombreProducto)
                    nombre.setText(nameProducto[1])
                    val descripcionProducto: List<String> = stringArray[3].split("=")
                    descripcion=view2.findViewById(R.id.descripcionProducto)
                    descripcion.setText(descripcionProducto[1])
                    val precioProducto: List<String> = stringArray[4].split("=")
                    precio=view2.findViewById(R.id.PrecioProducto)
                    val precioProductoSinLlave: List<String> = precioProducto[1].split("}")
                    val string=precioProductoSinLlave[0]
                    precio.setText("Precio: $ $string")


                    val imagenProducto: List<String> = stringArray[2].split("=")
                    val tamanio=(imagenProducto).size
                    enlace=imagenProducto[1]
                    for(num in 2..(tamanio-1)) {
                        enlace=enlace+"="+imagenProducto[num]
                    }

                    imageView=view2.findViewById(R.id.ImagenProducto)
                    Glide.with(this)
                        .load(enlace)
                        .into(imageView)

                    gallery.addView(view2)
                    botonEliminar=view2.findViewById(R.id.eliminar)
                    botonEliminar.setOnClickListener{
                        nompreProductoBorrar=nameProducto[1]
                        db.collection(nombrebase).document(nompreProductoBorrar).delete()
                        goToEditar()
                    }

                }
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }
    }

    fun goToEditar(){
        val action=editarCarritoDirections.editarCarritoSelf()
        view?.findNavController()?.navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editar_carrito, container, false)
    }



}
