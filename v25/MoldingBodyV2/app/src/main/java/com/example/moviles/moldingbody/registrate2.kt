package com.example.moviles.moldingbody


import android.content.ContentValues.TAG
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_registrate.*
import com.google.firebase.auth.FirebaseAuth
import android.widget.Toast.makeText as makeText1
import android.widget.Toast.makeText as makeText2
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseUser

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class registrate2 : Fragment() {
    var contador:Int=1
    var db = FirebaseFirestore.getInstance()

    internal lateinit var botonRegistraate: Button

    private lateinit var txtEmail:EditText
    private lateinit var password:EditText

    private lateinit var auth:FirebaseAuth
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        botonRegistraate=view.findViewById(R.id.registrar)
        botonRegistraate.setOnClickListener{saveData();createNewAccount();goToLogin()}


        txtEmail=view.findViewById(R.id.correo)
        password=view.findViewById(R.id.contrasenia)


        auth=FirebaseAuth.getInstance()

    }


    private fun createNewAccount(){
        val email:String=txtEmail.text.toString()
        val password:String=password.text.toString()
        if(!TextUtils.isEmpty(email)&&!TextUtils.isEmpty(password)){
            auth.createUserWithEmailAndPassword(email,password)
            val usuario:FirebaseUser?=auth.currentUser
            verifyEmail(usuario)
        }
    }

    private fun verifyEmail(user: FirebaseUser?){
        user?.sendEmailVerification()
            ?.addOnCompleteListener(OnCompleteListener(){
                    task ->
                if (task.isComplete){
                    Toast.makeText(context,"Email enviado", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(context,"Error al enviar el email", Toast.LENGTH_LONG).show()
                }
            })
    }

    fun goToLogin(){
        val action=registrate2Directions.registrate2Login()
        view?.findNavController()?.navigate(action)
    }

    fun saveData() {

        val user = datosUsuario(
            nombre.text.toString(),
            apellido.text.toString(),
            correo.text.toString(),
            contrasenia.text.toString(),
            contador
        )

        db.collection("usuario")
            .add(user)
            .addOnSuccessListener { documentReference ->
                Log.d(
                    TAG,
                    "DocumentSnapshot added with ID: " + documentReference.id
                )
            }
            .addOnFailureListener { e -> Log.w(TAG, "Error adding document", e) }

        contador++
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registrate2, container, false)
    }
}
