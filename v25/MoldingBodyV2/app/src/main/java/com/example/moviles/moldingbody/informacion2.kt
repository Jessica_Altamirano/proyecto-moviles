package com.example.moviles.moldingbody


import android.annotation.SuppressLint
import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.FirebaseFirestore
import android.widget.*
import androidx.core.app.NotificationCompat.getExtras
import com.bumptech.glide.Glide


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_INFO = "Informacion"

/**
 * A simple [Fragment] subclass.
 *
 */

const val ARG_NAME = "name"
const val ARG_BASE = "base"
lateinit var name:String
lateinit var nombrebase:String
class informacion2 : Fragment() {
    var contador:Int=1
    var db = FirebaseFirestore.getInstance()

    internal lateinit var gallery: LinearLayout
    internal lateinit var inflater: LayoutInflater
    internal lateinit var texto: TextView
    internal lateinit var imagen: ImageView
    internal lateinit var descipcion: TextView
    internal lateinit var precio: TextView
    internal lateinit var buttonAgregarCarrito: ImageButton
    internal lateinit var buttonM:Button
    internal lateinit var buttonL:Button

    internal lateinit var textView: TextView
    internal lateinit var imageView: ImageView
    internal lateinit var view2: View

    internal lateinit var datosTallaM: String
    var enlace:String=""
    internal lateinit var descripcionProd:TextView
    internal lateinit var precioProd:TextView

    companion object {
        fun newInstance(valor:String, base: String) :informacion2{
            val args = Bundle()
            args.putString(ARG_NAME, valor)
            args.putString(ARG_BASE, base)
            name=valor
            nombrebase=base
            val fragment = informacion2()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        gallery= view.findViewById(R.id.product)
        inflater =LayoutInflater.from(context)
        buttonM= view.findViewById(R.id.tallam)
        buttonL= view.findViewById(R.id.tallal)

        textView=view.findViewById(R.id.nombreProducto)
        imageView=view.findViewById(R.id.imageProducto)
        precio=view.findViewById(R.id.PrecioProducto)
        descipcion=view.findViewById(R.id.descripcionProducto)


        buttonAgregarCarrito=view.findViewById(R.id.buttonComprar)
        buttonAgregarCarrito.setOnClickListener{
            saveDataProducto()
            Toast.makeText(context,"Producto Agregado", Toast.LENGTH_LONG).show()
        }

        buttonM.setOnClickListener {tallam() }
        buttonL.setOnClickListener {tallal() }
        Log.d("name", name)
        mostrar()
    }


    @SuppressLint("SetTextI18n")
    fun mostrar(){
        val stringArray: List<String> = name.split(",")

        textView.setText(stringArray[1])

        Glide.with(this)
            .load(stringArray[0])
            .into(imageView)
        precio.text = "Precio: $"+stringArray[2]
        descipcion.setText(stringArray[3])
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Log.d("ok", name)
        return inflater.inflate(R.layout.fragment_informacion2, container, false)
    }


    fun saveDataProducto(){
        val stringArray: List<String> = name.split(",")
        val producto = datosProducto(
            stringArray[1],
            stringArray[0],
            stringArray[2].toDouble(),
            stringArray[3],
            contador
        )
        db.collection(nombrebase).document(stringArray[1]).set(producto)
            .addOnSuccessListener { documentReference ->
                Log.d(
                    ContentValues.TAG,
                    "DocumentSnapshot added with ID: " + documentReference
                )
            }
            .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error adding document", e) }

        contador++
    }

    fun tallam(){

        val producto="productosTallaM"
        db.collection(producto)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d(ContentValues.TAG, "${document.id} => ${document.data}")
                    datos=document.data.toString()
                    Log.d("Productos", datos)
                    val stringArray: List<String> = datos.split(",")
                    view2=inflater.inflate(R.layout.item,gallery, false)
                    val nameProducto: List<String> = stringArray[0].split("=")
                    texto=view2.findViewById(R.id.textoImagen)
                    texto.setText(nameProducto[1])
                    val nombre=nameProducto[1]

                    val descripcionProducto: List<String> = stringArray[2].split("=")
                    val descripcionProd = descripcionProducto[1]
                    val precioProducto: List<String> = stringArray[3].split("=")
                    val precioProductoSinLlave: List<String> = precioProducto[1].split("}")
                    val precioProd=precioProductoSinLlave[0]


                    val imagenProducto: List<String> = stringArray[1].split("=")
                    val tamanio=(imagenProducto).size
                    enlace=imagenProducto[1]
                    for(num in 2..(tamanio-1)) {
                        enlace=enlace+"="+imagenProducto[num]
                    }
                    imagen=view2.findViewById(R.id.imagenProducto)
                    Glide.with(this)
                        .load(enlace)
                        .into(imagen)
                    val link=enlace
                    imagen.setOnClickListener{
                        name="$link,$nombre,$precioProd,$descripcionProd"
                        mostrar()
                        gallery.removeAllViews()
                        gallery.refreshDrawableState()}
                    gallery.addView(view2)
                }
        }
    }

    fun tallal(){

        val producto="productosTallaL"
        db.collection(producto)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d(ContentValues.TAG, "${document.id} => ${document.data}")
                    datos=document.data.toString()
                    Log.d("Productos", datos)
                    val stringArray: List<String> = datos.split(",")
                    view2=inflater.inflate(R.layout.item,gallery, false)
                    val nameProducto: List<String> = stringArray[0].split("=")
                    texto=view2.findViewById(R.id.textoImagen)
                    texto.setText(nameProducto[1])
                    val nombre=nameProducto[1]

                    val descripcionProducto: List<String> = stringArray[2].split("=")
                    val descripcionProd = descripcionProducto[1]
                    val precioProducto: List<String> = stringArray[3].split("=")
                    val precioProductoSinLlave: List<String> = precioProducto[1].split("}")
                    val precioProd=precioProductoSinLlave[0]


                    val imagenProducto: List<String> = stringArray[1].split("=")
                    val tamanio=(imagenProducto).size
                    enlace=imagenProducto[1]
                    for(num in 2..(tamanio-1)) {
                        enlace=enlace+"="+imagenProducto[num]
                    }
                    imagen=view2.findViewById(R.id.imagenProducto)
                    Glide.with(this)
                        .load(enlace)
                        .into(imagen)
                    val link=enlace
                    imagen.setOnClickListener{
                        name="$link,$nombre,$precioProd,$descripcionProd"
                        mostrar()
                        gallery.removeAllViews()
                        gallery.refreshDrawableState()}
                    gallery.addView(view2)
                }
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }
    }
}
