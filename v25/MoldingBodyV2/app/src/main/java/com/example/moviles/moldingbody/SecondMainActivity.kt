package com.example.moviles.moldingbody

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.navArgs
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class SecondMainActivity : AppCompatActivity() {


    val args: SecondMainActivityArgs by navArgs()


    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        var nav = findNavController(R.id.nav_graph_menu)
        when (item.itemId) {
            R.id.informacion2 -> {
                //val informacionFragment = informacion2.newInstance()
                //openFragment(informacionFragment)
                nav.navigate(R.id.informacion2)
                return@OnNavigationItemSelectedListener true
            }

            R.id.agregarCarrito -> {
                //val agregarFragment = agregarCarrito.newInstance()
                //openFragment(agregarFragment)
                nav.navigate(R.id.agregarCarrito)
                return@OnNavigationItemSelectedListener true
            }
            R.id.editarCarrito -> {
                //val editarFragment = editarCarrito.newInstance()
                //openFragment(editarFragment)
                nav.navigate(R.id.editarCarrito)
                return@OnNavigationItemSelectedListener true
            }
            R.id.eliminarCarrito -> {
                //val eliminarFragment = eliminarCarrito.newInstance()
                //openFragment(eliminarFragment)
                nav.navigate(R.id.eliminarCarrito)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_main)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)


        val navController = findNavController(R.id.mainNavigationFragment)
        navView.setupWithNavController(navController)

        val fragment = informacion2.newInstance(args.informacionProducto,args.nombreBase)
        val fragment2 = agregarCarrito.newInstance(args.nombreBase)
        val fragment3 = editarCarrito.newInstance(args.nombreBase)
        val fragment4 = eliminarCarrito.newInstance(args.nombreBase)
    }

}
