package com.example.moviles.moldingbody

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class SecondMainActivity : AppCompatActivity() {


    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.agregarCarrito -> {
                val agregarFragment = agregarCarrito.newInstance()
                openFragment(agregarFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.editarCarrito -> {
                val editarFragment = editarCarrito.newInstance()
                openFragment(editarFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.eliminarCarrito -> {
                val eliminarFragment = eliminarCarrito.newInstance()
                openFragment(eliminarFragment)
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_main)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)


        val navController = findNavController(R.id.mainNavigationFragment)
        navView.setupWithNavController(navController)
    }
    private fun openFragment(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
