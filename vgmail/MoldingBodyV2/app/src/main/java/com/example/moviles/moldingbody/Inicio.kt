package com.example.moviles.moldingbody


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Inicio : Fragment() {

    internal lateinit var botonRegistrate: Button
    internal lateinit var botonLogin: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        botonRegistrate=view.findViewById(R.id.registrarse)
        botonRegistrate.setOnClickListener{goToRegistrate()}

        botonLogin=view.findViewById(R.id.inicioSesion)
        botonLogin.setOnClickListener{goToInicioSesion()}
    }
    fun goToRegistrate(){
        val action=InicioDirections.inicioRegistrate()
        view?.findNavController()?.navigate(action)
    }

    fun goToInicioSesion(){
        val action=InicioDirections.iniciologin()
        view?.findNavController()?.navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inicio, container, false)
    }


}
