package com.example.moviles.moldingbody
data class datosUsuario(val nombre: String = "", val apellido: String = "",val correo: String = "",val contrasenia: String = "", val idUsuario: Int = 0) {

    override fun toString() = nombre + "\t" + apellido + "\t" + correo+ "\t" + contrasenia+ "\t" + idUsuario
}
