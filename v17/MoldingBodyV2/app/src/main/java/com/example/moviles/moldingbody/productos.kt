package com.example.moviles.moldingbody


import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import android.widget.ArrayAdapter as ArrayAdapter1
import com.example.moviles.moldingbody.productos as productos1
import android.content.Intent as Intent1
import android.content.Intent as Intent2
import android.content.Intent as Intent3


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class productos : Fragment() {

    private val args: productosArgs by navArgs()

    internal lateinit var gallery: LinearLayout
    internal lateinit var inflater: LayoutInflater

    internal lateinit var galleryHombres: LinearLayout
    internal lateinit var inflaterHombres: LayoutInflater

    internal lateinit var galleryMujer: LinearLayout
    internal lateinit var inflaterMujer: LayoutInflater


    internal lateinit var textView: TextView
    internal lateinit var imageView: ImageView
    internal lateinit var view2: View
    internal lateinit var view3: View
    internal lateinit var view4: View

    internal lateinit var datos: String
    internal lateinit var nombreBase: String

    @SuppressLint("SetTextI18n", "PrivateResource")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        gallery= view.run { return@run this.findViewById(R.id.product) }
        inflater =LayoutInflater.from(context)

        val promociones: Array<String> = arrayOf("https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F1.jpg?alt=media&token=afe8664b-79b6-4729-a8b1-efde11d6f916,Faja cintura color negro,1,Faja elaborada con material de algodón, tejido transpirable e hipoalergénicos. Viene con un diseño con cintura alta para comprimir el abdomen, y sin costuras para brindar mayor comodidad",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F2.jpg?alt=media&token=4c600d7f-a84c-4fa6-959e-625cbe56b67d,Faja deportiva,2,fabricada con material de algodón y spandex que permite la transpiración y además es hipoalergénico",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F3.jpg?alt=media&token=673064ee-8d3a-4e67-bd71-152e9c6cefd1,Corsé Eleady,3,Faja elaborada con material de algodón, costuras reforzadas, tirantes y cintura a medio a muslo. Está diseñada para controlar el abdomen y la cintura",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F4.jpg?alt=media&token=3576d0bc-fc5f-4eec-89a4-98a076b2db76,Faja moldeadora,4,Diseño cómodo y práctico que forma el vientre tensando y encogiendo el estómago.",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F5.jpg?alt=media&token=86d727a6-a63e-4513-8b86-b037eac72d82,Faja tipo panties,5,Esta faja ha sido elaborada en licra spandex y powernet en su cara externa, materiales que moldean tu figura y se adaptan a tu cuerpo",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F6.jpg?alt=media&token=ad736068-7613-4213-bb89-3ac9144419a0,Faja postparto,6,Esta faja ha sido elaborada en material powernet en su parte externa y un forro de algodón en su parte interna, materiales que juntos hacen de esta prenda una faja suave y elástica.",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F7.jpg?alt=media&token=de9475c9-6d9f-4d3f-a207-b022f26a4e07,Faja espaldar,7,Estas fajas están hechas en powernet en toda su cara externa. Su función es comprimir y definir las curvas naturales de tu cuerpo",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F8.jpg?alt=media&token=9835aeac-04e9-4e3c-985e-a51c76e41d33,Licra deportiva moldeadora,8,Esta faja reducirá y moldeará tu cintura y abdomen, al mismo tiempo que levanta tus glúteos y le dan soporte a tu espalda.")


        for ((posicion,valor) in promociones.withIndex()) {
            val stringArray: List<String> = valor.split(",")
            view2=inflater.inflate(R.layout.item,gallery, false)
            textView=view2.findViewById(R.id.textoImagen)
            textView.setText(stringArray[1])
            imageView=view2.findViewById(R.id.imagenProducto)
            Glide.with(this)
                .load(stringArray[0])
                .into(imageView)

            //datos=valor

            imageView.setOnClickListener{datos=valor;goToInformacion()}
            gallery.addView(view2)
        }


        galleryHombres= view.run { findViewById(R.id.galleryHombre) }
        inflaterHombres =LayoutInflater.from(context)
         val fajasHombre: Array<String> = arrayOf("https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F1.jpg?alt=media&token=672af413-f24d-4dbd-8245-a33f8ae794d1, TopTie Shaper,1,Estas camisetas proporcionan un efecto de adelgazamiento inmediato, mientras lo llevas debajo de todo tipo de prendas de vestir.",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F2.jpg?alt=media&token=341a0a87-c529-469f-bf5c-984a13bd5bd5,Compresión extrema,2,Las fajas reductoras de compresión que conforman el cuerpo de Shaxea, te hacen ver tonificado y definido",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F5.jpg?alt=media&token=764a5336-f222-489f-8d2d-d7aab40ae206,Chaleco de neopreno,3,La compresión y la actividad térmica son las características esenciales de esta ropa interior: chaleco de tipo sauna; aumenta la transpiración y quema el exceso de calorías",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.2.jpg?alt=media&token=a07dc023-e902-4bc4-a901-a6318f6eebb7,Slim Shapewear,4,Estos chalecos de entrenamiento maximizan los resultados de pérdida de grasa y tonificación muscular que estás buscando, a través de la compresión y el proceso de sudoración",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.5.jpg?alt=media&token=9bc992e3-11a7-42e8-bab8-0ddf72e60b12,Shape Concept,5,Dos capas de tela de red elástica, hecho de nylon de alta calidad, 90% Poliéster y 10% Spandex, que se comprimen desde el pecho hasta el abdomen",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.6.jpg?alt=media&token=c092dcb9-99ca-489e-a239-9660642d5e55,Chaleco Malla Burvogue,6,Esta faja es una prenda de compresión de post-cirugía y uso cotidiano de fajas estéticas. ",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.63.jpg?alt=media&token=83973699-833c-4b8d-990b-3ece2975a818,Chaleco Reductor - Leo,7,Faja que controla todo el abdomen, y además de ser el soporte adicional que necesitas durante los entrenamientos o trabajo duro",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.664.jpg?alt=media&token=984b92de-f79f-4d44-b27a-560e2d245a32,Cinturón HLGO,8,Esta faja de compresión proporciona un adelgazamiento ergonómico, le da forma a tu pecho reduciendo la apariencia de la ginecomastia")
        for ((posicion,valor) in fajasHombre.withIndex()) {
            val stringArray: List<String> = valor.split(",")
            view3=inflaterHombres.inflate(R.layout.item,galleryHombres, false)
            textView=view3.findViewById(R.id.textoImagen)
            textView.setText(stringArray[1])
            imageView=view3.findViewById(R.id.imagenProducto)
            Glide.with(this)      
               .load(stringArray[0])
               .into(imageView)

            //datos=valor

            imageView.setOnClickListener{datos=valor;goToInformacion()}
            galleryHombres.addView(view3)
        }


        galleryMujer= view.run { findViewById(R.id.galleryMujer) }
        inflaterMujer =LayoutInflater.from(context)

        val fajasMujer: Array<String> = arrayOf("https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F2-colores-mujeres-sexy-corse%CC%81-de-la-talladora-mas-pechugona-mujeres-body-shapers-corse%CC%81s-fajas-de-cintura-de-la-talladora-del-corse%CC%81-de-la-talladora-caliente.jpg?alt=media&token=0532028a-8ed2-4dae-afec-662b04c69e2a,Corset sport Sayfut,1,Está prenda garantiza un mejor control de barriga y cintura, ajustándose con comodidad a tu cuerpo y resaltando tus curvas.",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F2.jpeg?alt=media&token=7dace815-fad1-4bb0-a710-78f635317d71,moldeadora de cintura Gotoly,2,Faja adelgazante elaborada con tela de algodón y spandex. Está diseñada con tirantes extraíbles y sin costura para garantizar mayor control de barriga, la cintura y las caderas.",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F4.jpeg?alt=media&token=029a376a-3653-4ce4-8407-52b75776402d,plus size All about Shapewear,3,Esta faja adelgazará tu abdomen, definirá tu cintura, levantará tus senos y dará soporte a tu espalda. Un modelo tipo sport, ideal para usar mientras te ejercitas.",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F5.jpg?alt=media&token=cac4da14-9e5e-4af1-86bf-02e1f837085c,Faja reductora posparto Diane & Geordi,4,Esta faja consiste en un chaleco térmico que adelgaza la cintura y el abdomen; su diseño de corte alto brinda un  refuerzo adicional que levanta y resalta la forma de tu pecho",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F6.jpeg?alt=media&token=404a985e-3a58-40a2-b3cf-66da71c4cd7e,Faja a la cintura Salome,5,Esta faja no solo reduce y moldea tu cintura y abdomen;es ideal como faja ortopédica durante el proceso de recuperación postoperatorio y postparto",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F7.jpg?alt=media&token=208be33f-7965-4431-9eb9-401da9804d9f,Corsé de neopreno,6,Body Shaper fabricada con material de algodón y spandex que permite la transpiración y además es hipoalergénico. Tiene un diseño sin costuras, sin tirantes y con un corte de cintura alta.",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F9.jpg?alt=media&token=f92b29da-7e66-47e5-b062-470700d8e50d,Faja deportiva de látex YIANNA,7,Faja adelgazante elaborada con tela de algodón y spandex. Está diseñada con tirantes extraíbles y sin costura para garantizar mayor control de barriga, la cintura y las caderas.",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F96245-mujeres-tummy-control-de-underbust-fajas-ropa-interior-que-adelgaza-faja-reductora-de-control-firme-body.jpg?alt=media&token=d528ddd0-dae1-4b90-b256-ad93df9b92ac,Faja a la cintura WMZD,8,Faja fabricada con material de licra que permite un interior fresco y suave; además, viene con tirantes extraíbles y costuras planas para evitar que se marque bajo la ropa")
        for ((posicion,valor) in fajasMujer.withIndex()) {
            val stringArray: List<String> = valor.split(",")
            view4=inflaterMujer.inflate(R.layout.item,galleryMujer, false)
            textView=view4.findViewById(R.id.textoImagen)
            textView.setText(stringArray[1])
            imageView=view4.findViewById(R.id.imagenProducto)
            Glide.with(this)
               .load(stringArray[0])
               .into(imageView)

            //datos=valor
            imageView.setOnClickListener{datos=valor;goToInformacion()}
            galleryMujer.addView(view4)
        }
    }
    fun goToInformacion(){
        nombreBase=args.nombreBase
        Log.d("producto", nombreBase)
        val action=productosDirections.productoInformacion(datos,nombreBase)
        view?.findNavController()?.navigate(action)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_productos, container, false)
    }


}
