package com.example.moviles.moldingbody


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_informacion.*
import kotlinx.android.synthetic.main.fragment_productos.*
import kotlinx.android.synthetic.main.item.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Informacion : Fragment() {

   /*internal lateinit var inflater: LayoutInflater

    internal lateinit var textView: TextView
    internal lateinit var imageView: ImageView
    internal lateinit var view2: View

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        inflater = LayoutInflater.from(context)

            view2 = inflater.inflate(R.layout.fragment_productos, productoLayout, false)
            textView = view2.findViewById(R.id.tituloProducto)
            textView.setText(""+textView)
            imageView = view2.findViewById(R.id.imagenProducto)
            imageView.setImageResource(imageView)
    }*/

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_informacion, container, false)
    }


}
