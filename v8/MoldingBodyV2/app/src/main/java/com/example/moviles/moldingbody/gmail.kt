package com.example.moviles.moldingbody


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import androidx.annotation.NonNull




// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class gmail : Fragment() {

    private var firebaseAuth: FirebaseAuth? = null
    private var firebaseAuthListener: FirebaseAuth.AuthStateListener? = null
    internal lateinit var ingresarGmail: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        ingresarGmail=view.findViewById(R.id.ingresarGmail)
        ingresarGmail.setOnClickListener{goToProductos()}
    }

    //---------------------------------------------------------------------
    fun goToProductos(){
        val action=gmailDirections.gmailProductos()
        view?.findNavController()?.navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gmail, container, false)
    }


}
