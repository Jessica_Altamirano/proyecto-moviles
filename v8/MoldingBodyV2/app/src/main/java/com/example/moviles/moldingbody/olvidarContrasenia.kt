package com.example.moviles.moldingbody

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"



/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [olvidarContrasenia.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [olvidarContrasenia.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class olvidarContrasenia : Fragment() {
    // TODO: Rename and change types of parameters

    private lateinit var txtEmail: EditText
    internal lateinit var botonEnviar: Button

    private lateinit var auth: FirebaseAuth
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        txtEmail=view.findViewById(R.id.editTextEmail)
        botonEnviar=view.findViewById(R.id.buttonEnviar)
        botonEnviar.setOnClickListener{Enviar()}

        auth= FirebaseAuth.getInstance()

    }

    fun Enviar() {
        val correo= txtEmail.text.toString()

        if (!TextUtils.isEmpty(correo)){
            auth.sendPasswordResetEmail(correo)
                .addOnCompleteListener(OnCompleteListener {
                        task ->

                            if (task.isSuccessful){
                                //startActivity(Intent(context,login::class.java))
                                val action=olvidarContraseniaDirections.regresarLogin()
                                view?.findNavController()?.navigate(action)
                            }else{
                                Toast.makeText(context,"Error al enviar al correo", Toast.LENGTH_LONG).show()
                            } })
        }
    }










    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_olvidar_contrasenia, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */



}
