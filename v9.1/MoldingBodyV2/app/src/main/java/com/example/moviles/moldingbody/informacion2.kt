package com.example.moviles.moldingbody


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.content.Intent
import android.content.Intent.getIntent
import android.content.Intent.getIntent
import androidx.core.app.NotificationCompat.getExtras
import android.content.Intent.getIntent
import android.content.Intent.getIntent
import android.util.Log
import android.widget.*
import androidx.core.app.NotificationCompat.getExtras
import com.bumptech.glide.Glide


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_INFO = "Informacion"

/**
 * A simple [Fragment] subclass.
 *
 */

const val ARG_NAME = "name"
lateinit var name:String
class informacion2 : Fragment() {
    internal lateinit var textView: TextView
    internal lateinit var imageView: ImageView
    internal lateinit var buttonAgregarCarrito: ImageButton

    companion object {
        fun newInstance(valor:String) :informacion2{
            val args = Bundle()
            args.putString(ARG_NAME, valor)
            Log.d("LLEGO", valor) //si llega
            name=valor
            val fragment = informacion2()
            fragment.arguments = args
            return fragment
        }
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        buttonAgregarCarrito=view.findViewById(R.id.buttonComprar)
        buttonAgregarCarrito.setOnClickListener{
            Toast.makeText(context,"Producto Agregado", Toast.LENGTH_LONG).show()
        }

        val stringArray: List<String> = name.split(",")
        textView=view.findViewById(R.id.nombreProducto)
        textView.setText(stringArray[1])
        imageView=view.findViewById(R.id.imageProducto)
        Glide.with(this)
            .load(stringArray[0])
            .into(imageView)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Log.d("ok", name)
        return inflater.inflate(R.layout.fragment_informacion2, container, false)
    }

}
