package com.example.moviles.moldingbody


import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth


// TODO: Rename parameter arguments, choose names that match

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class login : Fragment() {

    internal lateinit var botonGmail: Button
    internal lateinit var  botonIngresar:Button

    private lateinit var txtUser:EditText
    private lateinit var txtPassword:EditText
    private lateinit var auth: FirebaseAuth
    private lateinit var txtForgetPassword:TextView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        botonGmail=view.findViewById(R.id.gmail)
        botonGmail.setOnClickListener{goToGmail()}

        botonIngresar=view.findViewById(R.id.ingresar)
        botonIngresar.setOnClickListener{loginUser()}

        txtUser=view.findViewById(R.id.Ususario)
        txtPassword=view.findViewById(R.id.Contrasenia)


        txtForgetPassword=view.findViewById(R.id.olvidarContrasenia)
        txtForgetPassword.setOnClickListener{olvidarContrasenia()}

        auth=FirebaseAuth.getInstance()
    }

    private fun loginUser(){
        val user:String=txtUser.text.toString()
        val password:String=txtPassword.text.toString()
        if(!TextUtils.isEmpty(user)&&!TextUtils.isEmpty(password)){
            auth.signInWithEmailAndPassword(user,password)
                .addOnCompleteListener(OnCompleteListener(){
                    task ->
                    if(task.isSuccessful){
                        goToProductos()
                    }else{
                        Toast.makeText(context,"Error en la autenticación", Toast.LENGTH_LONG).show()
                    }
                })
        }
    }

    fun goToGmail(){
        val action=loginDirections.loginGMAIL()
        view?.findNavController()?.navigate(action)
    }
    fun goToProductos(){
        val action=loginDirections.loginProductos()
        view?.findNavController()?.navigate(action)
    }

    fun olvidarContrasenia(){
        //startActivity(Intent(context,olvidarContrasenia::class.java))
       val action=loginDirections.olvidarContrasenia()
       view?.findNavController()?.navigate(action)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }


}
