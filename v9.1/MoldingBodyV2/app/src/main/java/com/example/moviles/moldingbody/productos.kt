package com.example.moviles.moldingbody


import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import android.widget.ArrayAdapter as ArrayAdapter1
import com.example.moviles.moldingbody.productos as productos1
import android.content.Intent as Intent1
import android.content.Intent as Intent2
import android.content.Intent as Intent3


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class productos : Fragment() {


    internal lateinit var gallery: LinearLayout
    internal lateinit var inflater: LayoutInflater

    internal lateinit var galleryHombres: LinearLayout
    internal lateinit var inflaterHombres: LayoutInflater

    internal lateinit var galleryMujer: LinearLayout
    internal lateinit var inflaterMujer: LayoutInflater


    internal lateinit var textView: TextView
    internal lateinit var imageView: ImageView
    internal lateinit var view2: View
    internal lateinit var view3: View
    internal lateinit var view4: View

    internal lateinit var datos: String

    @SuppressLint("SetTextI18n", "PrivateResource")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        gallery= view.run { return@run this.findViewById(R.id.gallery) }
        inflater =LayoutInflater.from(context)

        val promociones: Array<String> = arrayOf("https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F1.jpg?alt=media&token=afe8664b-79b6-4729-a8b1-efde11d6f916,Faja cintura color negro",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F2.jpg?alt=media&token=4c600d7f-a84c-4fa6-959e-625cbe56b67d,Faja deportiva",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F3.jpg?alt=media&token=673064ee-8d3a-4e67-bd71-152e9c6cefd1,Corsé Eleady",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F4.jpg?alt=media&token=3576d0bc-fc5f-4eec-89a4-98a076b2db76,Faja moldeadora",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F5.jpg?alt=media&token=86d727a6-a63e-4513-8b86-b037eac72d82,Faja tipo panties",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F6.jpg?alt=media&token=ad736068-7613-4213-bb89-3ac9144419a0,Faja postparto",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F7.jpg?alt=media&token=de9475c9-6d9f-4d3f-a207-b022f26a4e07,Faja espaldar",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F8.jpg?alt=media&token=9835aeac-04e9-4e3c-985e-a51c76e41d33,Licra deportiva moldeadora")
        /*for ((posicion,valor) in promociones.withIndex()) {
            view2=inflater.inflate(R.layout.item,gallery, false)
            textView=view2.findViewById(R.id.textoImagen)
            textView.setText("Promociones" +posicion)
            imageView=view2.findViewById(R.id.imagenProducto)
            Glide.with(this)
                .load(valor)
                .into(imageView)

            imageView.setOnClickListener{goToInformacion()}
            gallery.addView(view2)
        }*/


        for ((posicion,valor) in promociones.withIndex()) {
            val stringArray: List<String> = valor.split(",")
            view2=inflater.inflate(R.layout.item,gallery, false)
            textView=view2.findViewById(R.id.textoImagen)
            textView.setText(stringArray[1])
            imageView=view2.findViewById(R.id.imagenProducto)
            Glide.with(this)
                .load(stringArray[0])
                .into(imageView)

            //datos=valor

            imageView.setOnClickListener{datos=valor;goToInformacion()}
            gallery.addView(view2)
        }


        galleryHombres= view.run { findViewById(R.id.galleryHombre) }
        inflaterHombres =LayoutInflater.from(context)
         val fajasHombre: Array<String> = arrayOf("https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F1.jpg?alt=media&token=672af413-f24d-4dbd-8245-a33f8ae794d1, TopTie Shaper,1",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F2.jpg?alt=media&token=341a0a87-c529-469f-bf5c-984a13bd5bd5,Compresión extrema,2",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F5.jpg?alt=media&token=764a5336-f222-489f-8d2d-d7aab40ae206,Chaleco de neopreno,3",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.2.jpg?alt=media&token=a07dc023-e902-4bc4-a901-a6318f6eebb7,Slim Shapewear,4",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.5.jpg?alt=media&token=9bc992e3-11a7-42e8-bab8-0ddf72e60b12,Shape Concept,5",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.6.jpg?alt=media&token=c092dcb9-99ca-489e-a239-9660642d5e55,Chaleco Malla Burvogue,6",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.63.jpg?alt=media&token=83973699-833c-4b8d-990b-3ece2975a818,Chaleco Reductor - Leo,7",
             "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.664.jpg?alt=media&token=984b92de-f79f-4d44-b27a-560e2d245a32,Cinturón HLGO,8")
        for ((posicion,valor) in fajasHombre.withIndex()) {
            val stringArray: List<String> = valor.split(",")
            view3=inflaterHombres.inflate(R.layout.item,galleryHombres, false)
            textView=view3.findViewById(R.id.textoImagen)
            textView.setText(stringArray[1])
            imageView=view3.findViewById(R.id.imagenProducto)
            Glide.with(this)      
               .load(stringArray[0])
               .into(imageView)

            //datos=valor

            imageView.setOnClickListener{datos=valor;goToInformacion()}
            galleryHombres.addView(view3)
        }


        galleryMujer= view.run { findViewById(R.id.galleryMujer) }
        inflaterMujer =LayoutInflater.from(context)

        val fajasMujer: Array<String> = arrayOf("https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F2-colores-mujeres-sexy-corse%CC%81-de-la-talladora-mas-pechugona-mujeres-body-shapers-corse%CC%81s-fajas-de-cintura-de-la-talladora-del-corse%CC%81-de-la-talladora-caliente.jpg?alt=media&token=0532028a-8ed2-4dae-afec-662b04c69e2a,Corset sport Sayfut",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F2.jpeg?alt=media&token=7dace815-fad1-4bb0-a710-78f635317d71,moldeadora de cintura Gotoly",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F4.jpeg?alt=media&token=029a376a-3653-4ce4-8407-52b75776402d,plus size All about Shapewear",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F5.jpg?alt=media&token=cac4da14-9e5e-4af1-86bf-02e1f837085c,Faja reductora posparto Diane & Geordi",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F6.jpeg?alt=media&token=404a985e-3a58-40a2-b3cf-66da71c4cd7e,Faja a la cintura Salome",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F7.jpg?alt=media&token=208be33f-7965-4431-9eb9-401da9804d9f,Corsé de neopreno",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F9.jpg?alt=media&token=f92b29da-7e66-47e5-b062-470700d8e50d,Faja deportiva de látex YIANNA",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2F96245-mujeres-tummy-control-de-underbust-fajas-ropa-interior-que-adelgaza-faja-reductora-de-control-firme-body.jpg?alt=media&token=d528ddd0-dae1-4b90-b256-ad93df9b92ac,Faja a la cintura WMZD")
        for ((posicion,valor) in fajasMujer.withIndex()) {
            val stringArray: List<String> = valor.split(",")
            view4=inflaterMujer.inflate(R.layout.item,galleryMujer, false)
            textView=view4.findViewById(R.id.textoImagen)
            textView.setText(stringArray[1])
            imageView=view4.findViewById(R.id.imagenProducto)
            Glide.with(this)
               .load(stringArray[0])
               .into(imageView)

            //datos=valor
            imageView.setOnClickListener{datos=valor;goToInformacion()}
            galleryMujer.addView(view4)
        }
    }
    fun goToInformacion(){
        val action=productosDirections.productoInformacion(datos)
        view?.findNavController()?.navigate(action)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_productos, container, false)
    }


}
