package com.example.moviles.moldingbody


import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.FirebaseFirestore
import android.widget.*
import androidx.core.app.NotificationCompat.getExtras
import com.bumptech.glide.Glide


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_INFO = "Informacion"

/**
 * A simple [Fragment] subclass.
 *
 */

const val ARG_NAME = "name"
const val ARG_BASE = "base"
lateinit var name:String
lateinit var nombrebase:String
class informacion2 : Fragment() {
    var contador:Int=1
    var db = FirebaseFirestore.getInstance()

    internal lateinit var gallery: LinearLayout
    internal lateinit var inflater: LayoutInflater
    internal lateinit var texto: TextView
    internal lateinit var imagen: ImageView
    internal lateinit var descipcion: TextView
    internal lateinit var precio: TextView
    internal lateinit var buttonAgregarCarrito: ImageButton
    internal lateinit var buttonM:Button
    internal lateinit var buttonL:Button

    internal lateinit var textView: TextView
    internal lateinit var imageView: ImageView
    internal lateinit var view2: View

    internal lateinit var datosTallaM: String

    companion object {
        fun newInstance(valor:String, base: String) :informacion2{
            val args = Bundle()
            args.putString(ARG_NAME, valor)
            args.putString(ARG_BASE, base)
            name=valor
            nombrebase=base
            val fragment = informacion2()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        gallery= view.findViewById(R.id.product)
        inflater =LayoutInflater.from(context)
        buttonM= view.findViewById(R.id.tallam)
        buttonL= view.findViewById(R.id.tallal)

        textView=view.findViewById(R.id.nombreProducto)
        imageView=view.findViewById(R.id.imageProducto)
        precio=view.findViewById(R.id.PrecioProducto)
        descipcion=view.findViewById(R.id.descripcionProducto)


        buttonAgregarCarrito=view.findViewById(R.id.buttonComprar)
        buttonAgregarCarrito.setOnClickListener{
            saveDataProducto()
            Toast.makeText(context,"Producto Agregado", Toast.LENGTH_LONG).show()
        }

        buttonM.setOnClickListener {tallam() }
        buttonL.setOnClickListener {tallal() }
        Log.d("name", name)
        mostrar()
    }


    fun mostrar(){
        val stringArray: List<String> = name.split(",")

        textView.setText(stringArray[1])

        Glide.with(this)
            .load(stringArray[0])
            .into(imageView)
        precio.setText(stringArray[2])
        descipcion.setText(stringArray[3])
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Log.d("ok", name)
        return inflater.inflate(R.layout.fragment_informacion2, container, false)
    }


    fun saveDataProducto(){
        val stringArray: List<String> = name.split(",")
        val producto = datosProducto(
            stringArray[1],
            stringArray[0],
            stringArray[2].toDouble(),
            stringArray[3],
            contador
        )
        db.collection(nombrebase).document(stringArray[1]).set(producto)
            .addOnSuccessListener { documentReference ->
                Log.d(
                    ContentValues.TAG,
                    "DocumentSnapshot added with ID: " + documentReference
                )
            }
            .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error adding document", e) }

        contador++
    }

    fun tallam(){

        val tallaM: Array<String> = arrayOf("https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F1.jpg?alt=media&token=afe8664b-79b6-4729-a8b1-efde11d6f916,Faja cintura color negro,1,Faja elaborada con material de algodón, tejido transpirable e hipoalergénicos. Viene con un diseño con cintura alta para comprimir el abdomen, y sin costuras para brindar mayor comodidad",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F2.jpg?alt=media&token=4c600d7f-a84c-4fa6-959e-625cbe56b67d,Faja deportiva,2,fabricada con material de algodón y spandex que permite la transpiración y además es hipoalergénico",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F3.jpg?alt=media&token=673064ee-8d3a-4e67-bd71-152e9c6cefd1,Corsé Eleady,3,Faja elaborada con material de algodón, costuras reforzadas, tirantes y cintura a medio a muslo. Está diseñada para controlar el abdomen y la cintura",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F4.jpg?alt=media&token=3576d0bc-fc5f-4eec-89a4-98a076b2db76,Faja moldeadora,4,Diseño cómodo y práctico que forma el vientre tensando y encogiendo el estómago.",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F5.jpg?alt=media&token=86d727a6-a63e-4513-8b86-b037eac72d82,Faja tipo panties,5,Esta faja ha sido elaborada en licra spandex y powernet en su cara externa, materiales que moldean tu figura y se adaptan a tu cuerpo",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F6.jpg?alt=media&token=ad736068-7613-4213-bb89-3ac9144419a0,Faja postparto,6,Esta faja ha sido elaborada en material powernet en su parte externa y un forro de algodón en su parte interna, materiales que juntos hacen de esta prenda una faja suave y elástica.",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F7.jpg?alt=media&token=de9475c9-6d9f-4d3f-a207-b022f26a4e07,Faja espaldar,7,Estas fajas están hechas en powernet en toda su cara externa. Su función es comprimir y definir las curvas naturales de tu cuerpo",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2F8.jpg?alt=media&token=9835aeac-04e9-4e3c-985e-a51c76e41d33,Licra deportiva moldeadora,8,Esta faja reducirá y moldeará tu cintura y abdomen, al mismo tiempo que levanta tus glúteos y le dan soporte a tu espalda.")


        for ((posicion,valor) in tallaM.withIndex()) {
            val stringArray: List<String> = valor.split(",")
            view2=inflater.inflate(R.layout.item,gallery, false)
            texto=view2.findViewById(R.id.textoImagen)
            texto.setText(stringArray[1])
            imagen=view2.findViewById(R.id.imagenProducto)
            Glide.with(this)
                .load(stringArray[0])
                .into(imagen)

            imagen.setOnClickListener{ name=valor
                mostrar()
                gallery.removeAllViews()
                gallery.refreshDrawableState()}
            gallery.addView(view2)
        }
    }

    fun tallal(){

        val tallaL: Array<String> = arrayOf("https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F1.jpg?alt=media&token=672af413-f24d-4dbd-8245-a33f8ae794d1, TopTie Shaper,1,Estas camisetas proporcionan un efecto de adelgazamiento inmediato mientras lo llevas debajo de todo tipo de prendas de vestir",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2Fgm1.jpg?alt=media&token=f6ca1563-0009-47b0-8930-924b4cf1bca4,Faja Tiro Alto,30,Esta faja adelgazará tu abdomen definirá tu cintura levantará tus senos",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasMujer%2Fgm2.jpg?alt=media&token=492a96a8-1d49-430f-bbb0-a0891aaf5a1a, Faja a la cintura,25,Esta faja moldea tu cintura y abdomen es ideal como faja ortopedica",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F2.jpg?alt=media&token=341a0a87-c529-469f-bf5c-984a13bd5bd5,Compresión extrema,2,Las fajas reductoras de compresión que conforman el cuerpo de Shaxea te hacen ver tonificado y definido",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F5.jpg?alt=media&token=764a5336-f222-489f-8d2d-d7aab40ae206,Chaleco de neopreno,3,La compresión y la actividad térmica son las características esenciales de esta ropa interior chaleco de tipo sauna",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.2.jpg?alt=media&token=a07dc023-e902-4bc4-a901-a6318f6eebb7,Slim Shapewear,4,Estos chalecos de entrenamiento maximizan los resultados de pérdida de grasa y tonificación muscular que estás buscando a través de la compresión y el proceso de sudoración",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.5.jpg?alt=media&token=9bc992e3-11a7-42e8-bab8-0ddf72e60b12,Shape Concept,5,Dos capas de tela de red elástica hecho de nylon de alta calidad 90% Poliéster y 10% Spandex que se comprimen desde el pecho hasta el abdomen",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.6.jpg?alt=media&token=c092dcb9-99ca-489e-a239-9660642d5e55,Chaleco Malla Burvogue,6,Esta faja es una prenda de compresión de post-cirugía y uso cotidiano de fajas estéticas",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.63.jpg?alt=media&token=83973699-833c-4b8d-990b-3ece2975a818,Chaleco Reductor - Leo,7,Faja que controla todo el abdomen y además de ser el soporte adicional que necesitas durante los entrenamientos o trabajo duro",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/FajasHombre%2F6.664.jpg?alt=media&token=984b92de-f79f-4d44-b27a-560e2d245a32,Cinturón HLGO,8,Esta faja de compresión proporciona un adelgazamiento ergonómico le da forma a tu pecho reduciendo la apariencia de la ginecomastia",
            "https://firebasestorage.googleapis.com/v0/b/moldingbody2.appspot.com/o/Promociones%2Fgm3.jpg?alt=media&token=51fb25ef-c1b2-4782-92ef-00a04ae616ab,Faja Panties,20,Esta faja ha sido elaborada en material powernet en su parte externa")


        for ((posicion,valor) in tallaL.withIndex()) {
            val stringArray: List<String> = valor.split(",")
            view2=inflater.inflate(R.layout.item,gallery, false)
            texto=view2.findViewById(R.id.textoImagen)
            texto.setText(stringArray[1])
            imagen=view2.findViewById(R.id.imagenProducto)
            Glide.with(this)
                .load(stringArray[0])
                .into(imagen)

            imagen.setOnClickListener{ name=valor
                mostrar()
                gallery.removeAllViews()
                gallery.refreshDrawableState()}
            gallery.addView(view2)
        }
    }
}
