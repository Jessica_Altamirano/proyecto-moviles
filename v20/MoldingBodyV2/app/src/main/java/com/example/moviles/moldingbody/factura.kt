package com.example.moviles.moldingbody

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class test : Fragment() {


    internal lateinit var gallery: LinearLayout
    internal lateinit var inflater: LayoutInflater
    internal lateinit var view2: View
    internal lateinit var nombre: TextView

    internal lateinit var precio: TextView
    internal var subtotalFactura: Double=0.0
    internal var ivaFactura: Double=0.0
    internal var totalFactura: Double=0.0

    internal lateinit var subtotal: TextView
    internal lateinit var iva: TextView
    internal lateinit var total: TextView

    var db = FirebaseFirestore.getInstance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        gallery= view.findViewById(R.id.producto)

        subtotal= view.findViewById(R.id.subtotal)
        iva= view.findViewById(R.id.iva)
        total= view.findViewById(R.id.total)

        inflater =LayoutInflater.from(context)

        db.collection(nombrebase)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d(ContentValues.TAG, "${document.id} => ${document.data}")
                    datos=document.data.toString()

                    val stringArray: List<String> = datos.split(",")

                    view2=inflater.inflate(R.layout.formulariofactura,gallery, false)

                    Log.d("ENLACE", datos)

                    val nameProducto: List<String> = stringArray[0].split("=")
                    nombre=view2.findViewById(R.id.nombreFactura)
                    nombre.setText(nameProducto[1])
                    val precioProducto: List<String> = stringArray[4].split("=")
                    precio=view2.findViewById(R.id.precioFactura)
                    val precioProductoSinLlave: List<String> = precioProducto[1].split("}")
                    precio.setText(precioProductoSinLlave[0])
                    val precioProductoSinPunto: List<String> = precioProducto[1].split(".")
                    subtotalFactura += (precioProductoSinPunto[0]).toDouble()
                    Log.d("Subtotal", subtotalFactura.toString())
                    gallery.addView(view2)
                }
                subtotal.text = subtotalFactura.toString()
                ivaFactura= (subtotalFactura*0.12)
                iva.text=ivaFactura.toString()
                totalFactura=subtotalFactura+ivaFactura
                total.text=totalFactura.toString()
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_factura, container, false)
    }
}
