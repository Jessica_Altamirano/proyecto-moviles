package com.example.moviles.moldingbody

data class datosProducto(val nombre: String = "", val imagen: String = "", val precio:Double, val idProducto: Int = 0) {

        override fun toString() = nombre + "\t" + imagen + "\t" + precio+ "\t" + idProducto
}
