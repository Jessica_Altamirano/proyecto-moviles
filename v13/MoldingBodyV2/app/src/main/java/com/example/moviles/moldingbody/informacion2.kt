package com.example.moviles.moldingbody


import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.FirebaseFirestore
import android.widget.*
import androidx.core.app.NotificationCompat.getExtras
import com.bumptech.glide.Glide


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_INFO = "Informacion"

/**
 * A simple [Fragment] subclass.
 *
 */

const val ARG_NAME = "name"
const val ARG_BASE = "base"
lateinit var name:String
lateinit var nombrebase:String
class informacion2 : Fragment() {
    var contador:Int=1
    var db = FirebaseFirestore.getInstance()

    internal lateinit var textView: TextView
    internal lateinit var imageView: ImageView
    internal lateinit var descipcion: TextView
    internal lateinit var buttonAgregarCarrito: ImageButton

    companion object {
        fun newInstance(valor:String, base: String) :informacion2{
            val args = Bundle()
            args.putString(ARG_NAME, valor)
            args.putString(ARG_BASE, base)
            name=valor
            nombrebase=base
            val fragment = informacion2()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        buttonAgregarCarrito=view.findViewById(R.id.buttonComprar)
        buttonAgregarCarrito.setOnClickListener{
            saveDataProducto()
            Toast.makeText(context,"Producto Agregado", Toast.LENGTH_LONG).show()
        }

        val stringArray: List<String> = name.split(",")
        textView=view.findViewById(R.id.nombreProducto)
        textView.setText(stringArray[1])
        imageView=view.findViewById(R.id.imageProducto)
        Glide.with(this)
            .load(stringArray[0])
            .into(imageView)
        descipcion=view.findViewById(R.id.descripcionProducto)
        descipcion.setText(stringArray[2])
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Log.d("ok", name)
        return inflater.inflate(R.layout.fragment_informacion2, container, false)
    }


    fun saveDataProducto(){
        val stringArray: List<String> = name.split(",")
        val producto = datosProducto(
            stringArray[1],
            stringArray[0],
            stringArray[2].toDouble(),
            contador
        )
        db.collection(nombrebase).document(stringArray[1]).set(producto)
            .addOnSuccessListener { documentReference ->
                Log.d(
                    ContentValues.TAG,
                    "DocumentSnapshot added with ID: " + documentReference
                )
            }
            .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error adding document", e) }

        contador++
    }
}
