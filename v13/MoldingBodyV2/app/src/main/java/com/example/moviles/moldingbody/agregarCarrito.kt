package com.example.moviles.moldingbody


import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_registrate.*


lateinit var nombrebaseCarrito:String
lateinit var datos:String

/**
 * A simple [Fragment] subclass.
 *
 */
class agregarCarrito : Fragment() {

    internal lateinit var gallery: LinearLayout
    internal lateinit var inflater: LayoutInflater
    internal lateinit var view2: View
    internal lateinit var nombre: TextView
    internal lateinit var descripcion: TextView
    internal lateinit var precio: TextView
    internal lateinit var imageView: ImageView

    var db = FirebaseFirestore.getInstance()
    var enlace:String=""


    companion object {
        fun newInstance( base: String) :agregarCarrito{
            val args = Bundle()
            args.putString(ARG_BASE, base)
            nombrebaseCarrito=base
            //Log.d("CARRITO", nombrebaseCarrito)
            val fragment = agregarCarrito()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        gallery= view.findViewById(R.id.producto)
        inflater =LayoutInflater.from(context)


        db.collection(nombrebase)
        .get()
        .addOnSuccessListener { documents ->
            for (document in documents) {
                Log.d(TAG, "${document.id} => ${document.data}")
                datos=document.data.toString()
                    val stringArray: List<String> = datos.split(",")
                    view2=inflater.inflate(R.layout.carro,gallery, false)
                    val nameProducto: List<String> = stringArray[0].split("=")
                    nombre=view2.findViewById(R.id.nombreProducto)
                    nombre.setText(nameProducto[1])
                    //val descripcionProducto: List<String> = stringArray[0].split("=")
                    descripcion=view2.findViewById(R.id.descripcionProducto)
                    descripcion.setText("Funciono")
                    val precioProducto: List<String> = stringArray[3].split("=")
                    precio=view2.findViewById(R.id.PrecioProducto)
                    precio.setText(precioProducto[1])

                    val imagenProducto: List<String> = stringArray[1].split("=")
                    val tamanio=(imagenProducto).size
                    enlace=imagenProducto[1]
                    for(num in 2..(tamanio-1)) {
                       enlace=enlace+"="+imagenProducto[num]
                    }

                imageView=view2.findViewById(R.id.ImagenProducto)
                    Glide.with(this)
                        .load(enlace)
                        .into(imageView)

                    //imageView.setOnClickListener{datos=valor;goToInformacion()}
                    gallery.addView(view2)

            }
        }
        .addOnFailureListener { exception ->
            Log.w(TAG, "Error getting documents: ", exception)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_agregar_carrito, container, false)
    }

}
